# Show rotations of single qubit and dephasing in Bloch sphere representation

import numpy
import numpy.random
import sys
import pylab

sx=numpy.array([[0,1],[1,0]],dtype=complex)
sy=numpy.array([[0,-1.j],[1.j,0]],dtype=complex)
sz=numpy.array([[1,0],[0,-1]],dtype=complex)

# Hadamard gate - rotate from up to right
d_theta=numpy.pi/200.
theta=numpy.arange(0.,numpy.pi/2.+d_theta/2,d_theta)
data=numpy.zeros((len(theta),4)) # t, x, y, z
data[:,0]=numpy.arange(len(theta))
data[:,1]=numpy.sin(theta)
data[:,3]=numpy.cos(theta)
numpy.savetxt('single_qubit_rotate_z_to_x.csv',data,header='t,x,y,z',delimiter=',')

# Rotate in x-y plane without dephasing (10 loops)
d_theta=numpy.pi/200.
theta=numpy.arange(0.,20.*numpy.pi+d_theta/2,d_theta)
data=numpy.zeros((len(theta),4)) # t, x, y, z
data[:,0]=numpy.arange(len(theta))
data[:,1]=numpy.cos(theta)
data[:,2]=numpy.sin(theta)
numpy.savetxt('single_qubit_rotate_xy.csv',data,header='t,x,y,z',delimiter=',')

# Rotate in x-y plane with dephasing (10 loops)
d_theta=numpy.pi/200.
gamma=0.01
theta=numpy.arange(0.,20.*numpy.pi+d_theta/2,d_theta)
data=numpy.zeros((len(theta),4)) # t, x, y, z
data[:,0]=numpy.arange(len(theta))
data[:,1]=numpy.cos(theta)*numpy.exp(-data[:,0]*gamma)
data[:,2]=numpy.sin(theta)*numpy.exp(-data[:,0]*gamma)
numpy.savetxt('single_qubit_rotate_xy_with_dephasing.csv',data,header='t,x,y,z',delimiter=',')
