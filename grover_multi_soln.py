# Show implementation of Grover's algorithm for N qubits using Bloch sphere

import numpy
import numpy.random
import sys
import scipy.linalg

def exp_val(psi,obs):
    return numpy.real(psi.conjugate().transpose().dot(obs.dot(psi)))

def wfn(c_th,phi):
    cc=((1.+c_th)/2.)**0.5 # = cos(theta/2)
    ss=((1.-c_th)/2.)**0.5 # = sin(theta/2)
    return numpy.array([cc,ss*numpy.exp(1.j*phi)])

def calc_obs(psi,X_lst,Y_lst,Z_lst):
    vals=numpy.zeros((3*len(X_lst)))
    for site in range(len(X_lst)):
        vals[3*site]=exp_val(psi,X_lst[site])
        vals[3*site+1]=exp_val(psi,Y_lst[site])
        vals[3*site+2]=exp_val(psi,Z_lst[site])
    return vals

narg=2
if len(sys.argv) < narg:
    print('Usage: grover_multi_soln.py N_qubit N_soln [rand_seed]')
    sys.exit()

N=int(sys.argv[1])
N_soln=int(sys.argv[2])
if len(sys.argv) > 3:
    numpy.random.seed(int(sys.argv[3]))
target=numpy.random.choice(numpy.arange(2**N,dtype=int),size=(N_soln),replace=False)#(2**N,size=(N_soln))

X=numpy.array([[0,1],[1,0]],dtype=complex)
Y=numpy.array([[0,-1.j],[1.j,0]],dtype=complex)
Z=numpy.array([[1,0],[0,-1]],dtype=complex)
I=numpy.array([[1,0],[0,1]],dtype=complex)

X_lst=[]
Y_lst=[]
Z_lst=[]

for site in range(N):
    if site==0:
        XX=numpy.kron(X,I)
        YY=numpy.kron(Y,I)
        ZZ=numpy.kron(Z,I)
    elif site==1:
        XX=numpy.kron(I,X)
        YY=numpy.kron(I,Y)
        ZZ=numpy.kron(I,Z)
    else:
        XX=numpy.kron(I,I)
        YY=numpy.kron(I,I)
        ZZ=numpy.kron(I,I)
    for j in range(2,N):
        if site==j:
            XX=numpy.kron(XX,X)
            YY=numpy.kron(YY,Y)
            ZZ=numpy.kron(ZZ,Z)
        else:
            XX=numpy.kron(XX,I)
            YY=numpy.kron(YY,I)
            ZZ=numpy.kron(ZZ,I)
    X_lst.append(XX)
    Y_lst.append(YY)
    Z_lst.append(ZZ)

XplusZ_tot=(X_lst[0]+Z_lst[0])/2.0**0.5
for j in range(1,N):
    XplusZ_tot=XplusZ_tot+(X_lst[j]+Z_lst[j])/2.0**0.5

n_times_per_gate=16 # Number of time steps to break each gate into

# Create the n-th root gates
root_had=scipy.linalg.expm(-1.j*XplusZ_tot*numpy.pi/(2.*n_times_per_gate)) # pi rotation about the x+z axis
root_oracle=numpy.identity(2**N,dtype=complex)
for j in range(len(target)):
    root_oracle[target[j],target[j]]=numpy.exp(-1.j*numpy.pi/float(n_times_per_gate))
print('root_oracle=',numpy.diag(root_oracle))
root_zero_flip=numpy.identity(2**N,dtype=complex)
root_zero_flip[0,0]=numpy.exp(-1.j*numpy.pi/float(n_times_per_gate))

theta_grover=numpy.arcsin(2.0*(N_soln*float(2**N-N_soln))**0.5/2**N) # Rotation angle during each Grover step in Grover basis
n_grover_steps=int(round(numpy.pi/(2*theta_grover)))
n_gates_per_grover_step=4
n_times_per_grover_step=n_gates_per_grover_step*n_times_per_gate
n_times=n_times_per_grover_step*n_grover_steps + n_times_per_gate + 1 # Total number of time steps = all grover steps + initial hadamard + 1 for inclusive start + end
data=numpy.zeros((n_times,3*N+1)) # t, x1, y1, z1, x2, y2, z2, ...
data[:,0]=numpy.arange(data.shape[0])

psi=numpy.zeros((2**N),dtype=complex)
psi_t=numpy.zeros((2**N,data.shape[0]),dtype=complex) # |psi(t)> stored for later use
psi[0]=1.
psi_t[:,0]=psi
data[0,1:]=calc_obs(psi,X_lst,Y_lst,Z_lst)
data_ind=1

print('Starting initial Hadamard')

# Initial Hadamard
for j in range(n_times_per_gate):
    psi=root_had.dot(psi)
    psi_t[:,data_ind]=psi
    data[data_ind,1:]=calc_obs(psi,X_lst,Y_lst,Z_lst)
    data_ind+=1

# Then Grover steps
for grover_step in range(n_grover_steps):
    print('Starting Grover step '+str(grover_step)+' out of '+str(n_grover_steps))
    # Oracle
    # n_test=n_times_per_gate+0
    # if grover_step==2:
    #     n_test=n_times_per_gate-1
    for j in range(n_times_per_gate):
        psi=root_oracle.dot(psi)
        psi_t[:,data_ind]=psi
        data[data_ind,1:]=calc_obs(psi,X_lst,Y_lst,Z_lst)
        data_ind+=1
        
    # Hadamard
    for j in range(n_times_per_gate):
        psi=root_had.dot(psi)
        psi_t[:,data_ind]=psi
        data[data_ind,1:]=calc_obs(psi,X_lst,Y_lst,Z_lst)
        data_ind+=1

    # Zero "oracle"
    for j in range(n_times_per_gate):
        psi=root_zero_flip.dot(psi)
        psi_t[:,data_ind]=psi
        data[data_ind,1:]=calc_obs(psi,X_lst,Y_lst,Z_lst)
        data_ind+=1

    # Hadamard
    for j in range(n_times_per_gate):
        psi=root_had.dot(psi)
        psi_t[:,data_ind]=psi
        data[data_ind,1:]=calc_obs(psi,X_lst,Y_lst,Z_lst)
        data_ind+=1

fname='grover_'+str(N)+'_bloch.csv'
head='t'
form='%d'
for site in range(1,N+1):
    head=head+', x'+str(site)+', y'+str(site)+', z'+str(site)
    form=form+',%.19f,%.19f,%.19f'
numpy.savetxt(fname,data,header=head,fmt=form)

if sys.argv[-1] == 'show':
    import pylab

    for site in range(N):
        pylab.figure(site)
        pylab.clf()
        pylab.plot(data[:,0],data[:,3*site+1],label='x'+str(site+1))
        pylab.plot(data[:,0],data[:,3*site+2],label='y'+str(site+1))
        pylab.plot(data[:,0],data[:,3*site+3],label='z'+str(site+1))
        pylab.plot(data[:,0],(data[:,3*site+1]**2+data[:,3*site+2]**2+data[:,3*site+3]**2)**0.5,label='n'+str(site+1))
        pylab.legend(loc='best')
        pylab.ylim(-1,1)

    pylab.show()
