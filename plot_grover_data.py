# Show implementation of Grover's algorithm for N qubits using Bloch sphere

import numpy
import numpy.random
import sys
import scipy.linalg
import pylab

if len(sys.argv) < 2:
    print('Usage: plot_grover_data.py $fname')
    sys.exit()

fname=sys.argv[1]
data=numpy.loadtxt(fname,skiprows=1,delimiter=',')

for site in range(6):
    #    pylab.figure(site)
    #    pylab.clf()
    pylab.plot(data[:,0],data[:,3*site+1],label='x',linewidth=2)
    pylab.plot(data[:,0],data[:,3*site+2],label='y',linewidth=2)
    pylab.plot(data[:,0],data[:,3*site+3],label='z',linewidth=2)
    for j in range(16,data.shape[0],64):
        pylab.plot([j,j],[-1,1],'k--',linewidth=0.5)
    # pylab.plot(data[:,0],(data[:,3*site+1]**2+data[:,3*site+2]**2+data[:,3*site+3]**2)**0.5,label='n'+str(site+1))
    pylab.legend(loc='lower left')
    pylab.xlim(0,data.shape[0]-1)
    pylab.ylim(-1,1)
    pylab.savefig('grover_data_'+str(site)+'.png')
    pylab.show()

