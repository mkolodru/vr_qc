# Show implementation of entangling [H_1 then CNOT] followed by disentangling (same thing dagger) for 2 qubits using
# Bloch sphere and path integral

import numpy
import numpy.random
import scipy.linalg
import sys
import pylab

narg=3
if len(sys.argv) < narg:
    print('Usage: two_qubit_for_grant.py eps power [rand_seed]')
    sys.exit()

eps=float(sys.argv[1])
power=float(sys.argv[2]) # What power to raise probability to in determining trajectory
if len(sys.argv) > 3:
    numpy.random.seed(int(sys.argv[3]))

#numpy.seterr(all='print')
    
X=numpy.array([[0,1],[1,0]],dtype=complex)
Y=numpy.array([[0,-1.j],[1.j,0]],dtype=complex)
Z=numpy.array([[1,0],[0,-1]],dtype=complex)
I=numpy.array([[1,0],[0,1]],dtype=complex)

X1=numpy.kron(X,I)
Y1=numpy.kron(Y,I)
Z1=numpy.kron(Z,I)
X2=numpy.kron(I,X)
Y2=numpy.kron(I,Y)
Z2=numpy.kron(I,Z)

def exp_val(psi,obs):
    return numpy.real(psi.conjugate().transpose().dot(obs.dot(psi)))

def wfn(c_th,phi):
    cc=((1.+c_th)/2.)**0.5 # = cos(theta/2)
    ss=((1.-c_th)/2.)**0.5 # = sin(theta/2)
    return numpy.array([cc,ss*numpy.exp(1.j*phi)])

# Grid of points to sample path integral - sample cos(theta) and phi evenly...
n_g=16
d_c_th=1./float(n_g)
c_th_vals=numpy.arange(-1.+d_c_th/2.,1.,d_c_th)
phi_vals=numpy.arange(0.,2.*numpy.pi-0.01,2.*numpy.pi/float(n_g))
x_g=[]
y_g=[]
z_g=[]
phi_g=[]
for c_th in c_th_vals:
    s_th=(1.-c_th**2)**0.5
    for phi in phi_vals:
        x_g.append(s_th*numpy.cos(phi))
        y_g.append(s_th*numpy.sin(phi))
        z_g.append(c_th)
        phi_g.append(phi)
x_g=numpy.array(x_g)
y_g=numpy.array(y_g)
z_g=numpy.array(z_g) # Same as cos(theta)
phi_g=numpy.array(phi_g)

psi_g=numpy.zeros((len(x_g),len(x_g),4),dtype=complex) # Create wfn for each possible coherent state
for j in range(len(x_g)):
    for k in range(len(x_g)):
        psi_g[j,k,:]=numpy.kron(wfn(z_g[j],phi_g[j]),wfn(z_g[k],phi_g[k]))

n_theta=32
d_theta=numpy.pi/(2*n_theta)
theta=numpy.arange(0.,numpy.pi/2.+d_theta/2,d_theta) # Length n_theta + 1 going from 0 to pi/2
data=numpy.zeros((4*n_theta+1,7)) # t, x1, y1, z1, x2, y2, z2
data[:,0]=numpy.arange(data.shape[0])
psi_0=numpy.zeros((4),dtype=complex)
psi=numpy.zeros((4,data.shape[0]),dtype=complex) # |psi(t)>, with basis |00>, |01>, |10>, |11>
psi_0[0]=1.

# Hadamard gate - rotate from up to right on qubit 1
x_plus_z_norm=(X1+Z1)/2.0**0.5#numpy.kron(numpy.array([[1,1],[1,-1]],dtype=complex),numpy.identity(2))
for j in range(len(theta)):
    # Hadamard unitary is [cos(theta/2),sin(theta/2); -sin(theta/2),cos(theta/2)] for one qubit up to theta=pi/2
    # In 2-qubit with this choice of Kronecker, it is [c 0 s 0; 0 c 0 s; -s 0 c 0; 0 -s 0 c]
    #    cc=numpy.cos(theta[j]/2.)
    #    ss=numpy.sin(theta[j]/2.)
    #    U_H=numpy.array([[cc,0,-ss,0],[0,cc,0,-ss],[ss,0,cc,0],[0,ss,0,cc]])
    U_H=scipy.linalg.expm(-1.j*theta[j]*x_plus_z_norm)
    psi[:,j]=U_H.dot(psi_0)
    data[j,1]=exp_val(psi[:,j],X1)
    data[j,2]=exp_val(psi[:,j],Y1)
    data[j,3]=exp_val(psi[:,j],Z1)
    data[j,4]=exp_val(psi[:,j],X2)
    data[j,5]=exp_val(psi[:,j],Y2)
    data[j,6]=exp_val(psi[:,j],Z2)
psi_0=psi[:,n_theta] # Final state for later use

# Then do cnot gate as exp(i*theta)*exp(-i*X*theta) in lower 2x2 block
U_cnot=numpy.identity(4,dtype=complex)
for j in range(len(theta)):
    U_block=numpy.exp(1.j*theta[j])*(numpy.cos(theta[j])*I - 1.j*numpy.sin(theta[j])*X)
    U_cnot[2:,2:]=U_block
    psi[:,j+n_theta]=U_cnot.dot(psi_0)
    data[j+n_theta,1]=exp_val(psi[:,j+n_theta],X1)
    data[j+n_theta,2]=exp_val(psi[:,j+n_theta],Y1)
    data[j+n_theta,3]=exp_val(psi[:,j+n_theta],Z1)
    data[j+n_theta,4]=exp_val(psi[:,j+n_theta],X2)
    data[j+n_theta,5]=exp_val(psi[:,j+n_theta],Y2)
    data[j+n_theta,6]=exp_val(psi[:,j+n_theta],Z2)
psi_0=psi[:,2*n_theta] # Final state for later use

# Then do cnot gate as exp(i*theta)*exp(-i*X*theta) in lower 2x2 block
U_cnot=numpy.identity(4,dtype=complex)
for j in range(len(theta)):
    U_block=numpy.exp(1.j*theta[j])*(numpy.cos(theta[j])*I - 1.j*numpy.sin(theta[j])*X)
    U_cnot[2:,2:]=U_block
    psi[:,j+2*n_theta]=U_cnot.dot(psi_0)
    data[j+2*n_theta,1]=exp_val(psi[:,j+2*n_theta],X1)
    data[j+2*n_theta,2]=exp_val(psi[:,j+2*n_theta],Y1)
    data[j+2*n_theta,3]=exp_val(psi[:,j+2*n_theta],Z1)
    data[j+2*n_theta,4]=exp_val(psi[:,j+2*n_theta],X2)
    data[j+2*n_theta,5]=exp_val(psi[:,j+2*n_theta],Y2)
    data[j+2*n_theta,6]=exp_val(psi[:,j+2*n_theta],Z2)
psi_0=psi[:,3*n_theta] # Final state for later use

# Hadamard gate - rotate from up to right on qubit 1
for j in range(len(theta)):
    # Hadamard unitary is [cos(theta/2),sin(theta/2); -sin(theta/2),cos(theta/2)] for one qubit up to theta=pi/2
    # In 2-qubit with this choice of Kronecker, it is [c 0 s 0; 0 c 0 s; -s 0 c 0; 0 -s 0 c]
    #    cc=numpy.cos(theta[j]/2.)
    #    ss=numpy.sin(theta[j]/2.)
    #    U_H=numpy.array([[cc,0,-ss,0],[0,cc,0,-ss],[ss,0,cc,0],[0,ss,0,cc]])
    U_H=scipy.linalg.expm(-1.j*theta[j]*x_plus_z_norm)
    psi[:,j+3*n_theta]=U_H.dot(psi_0)
    data[j+3*n_theta,1]=exp_val(psi[:,j+3*n_theta],X1)
    data[j+3*n_theta,2]=exp_val(psi[:,j+3*n_theta],Y1)
    data[j+3*n_theta,3]=exp_val(psi[:,j+3*n_theta],Z1)
    data[j+3*n_theta,4]=exp_val(psi[:,j+3*n_theta],X2)
    data[j+3*n_theta,5]=exp_val(psi[:,j+3*n_theta],Y2)
    data[j+3*n_theta,6]=exp_val(psi[:,j+3*n_theta],Z2)

numpy.savetxt('two_qubit_bloch.csv',data,header='t, x1, y1, z1, x2, y2, z2, 32 steps per gate',fmt='%d,%.19f,%.19f,%.19f,%.19f,%.19f,%.19f')

if sys.argv[-1] == 'show':
    pylab.figure(3)
    pylab.clf()
    pylab.plot(data[:,0],data[:,1],label='x1')
    pylab.plot(data[:,0],data[:,2],label='y1')
    pylab.plot(data[:,0],data[:,3],label='z1')
    pylab.legend(loc='best')
    pylab.ylim(-1,1)
    pylab.savefig('plot_spin_1_bloch.png')
    
    pylab.figure(4)
    pylab.clf()
    pylab.plot(data[:,0],data[:,4],label='x2')
    pylab.plot(data[:,0],data[:,5],label='y2')
    pylab.plot(data[:,0],data[:,6],label='z2')
    pylab.legend(loc='best')
    pylab.ylim(-1,1)
    pylab.savefig('plot_spin_2_bloch.png')
    

# Now take this data and do a random sampling of the (approximate) path integral in spin coherent states
data=numpy.zeros((psi.shape[1],9)) # t, x1, y1, z1, x2, y2, z2, g1, g2
data[:,0]=numpy.arange(psi.shape[1])
# Start with both qubit states as close to north pole as possible, last point of grids
data[0,-2]=len(x_g)-1
data[0,-1]=len(x_g)-1

p=numpy.zeros((len(x_g)**2))
# eps=5.
# power=100 
for j in range(1,psi.shape[1]):
    g1_curr=int(data[j-1,-2])
    g2_curr=int(data[j-1,-1])
    print('g1_curr=',g1_curr)
    x1_curr=x_g[g1_curr]
    y1_curr=y_g[g1_curr]
    z1_curr=z_g[g1_curr]
    x2_curr=x_g[g2_curr]
    y2_curr=y_g[g2_curr]
    z2_curr=z_g[g2_curr]
    for g1B in range(len(x_g)):
        theta_1=numpy.arccos(max(min(x1_curr*x_g[g1B] + y1_curr*y_g[g1B] + z1_curr*z_g[g1B],1.),-1.))
        for g2B in range(len(x_g)):
            ind=g1B+g2B*len(x_g)
            # Starting from spin state (s1A,s2A)=(x1A,y1A,...) at j-1, calculated probability to sample (s1B,s2B)=(x1B,y1B,...) at j for each grid point
            # For now, use prob prop to |<s2A,s2B|psi>|^2 multiplied by a "kinetic damping" that prevents large jumps of each spin state
            # For kinetic damping, use prod_j[exp(-eps*tan^2(theta_j/2))], where j=1,2, eps ~ 100 is some damping constant (large means small jumps), and
            # theta_j=arccos(xjA*xjB + yjA*yjB + zjA*zjB) is the angle that spin j rotates during the time step. This is designed to completely suppress pi rotations,
            # and generally damp away from 0 rotations
            theta_2=numpy.arccos(max(min(x2_curr*x_g[g2B] + y2_curr*y_g[g2B] + z2_curr*z_g[g2B],1.),-1.))
            # if numpy.cos(theta_1) < -0.999 or numpy.cos(theta_2) < -0.999:
            #     p[ind]=0.
            # else:
            p[ind]=numpy.exp(-eps*(numpy.tan(theta_1/2.-1e-9)**2 + numpy.tan(theta_2/2.-1e-9)**2)) * abs(psi[:,j].conjugate().transpose().dot(psi_g[g1B,g2B,:]))**2
            #print('For g1B=',g1B,', g2B=',g2B,', psi=',psi[:,j],', psi_g[g1B,g2B,:]=',psi_g[g1B,g2B,:],', and p=',p[ind])

    p_mod=(p/max(p))**power
    #    p=p/numpy.sum(p)
    p_cum=numpy.cumsum(p_mod)/numpy.sum(p_mod)
    if abs(p_cum[-1] - 1.) > 1e-9:
        print('Probability is not normalized!')
        print('min(p)=',min(p))
        print('max(p)=',max(p))
        print('p_cum[:10]=',p_cum[:10])
        print('p_cum[-10:]=',p_cum[-10:])
        sys.exit()
    r_val=numpy.random.random()
    if power < 0.5:
        ind_new = numpy.argmax(p)
    else:
        ind_new=numpy.searchsorted(p_cum,r_val)
    g1B=ind_new % len(x_g)
    g2B=int(ind_new // len(x_g))

    # if j==1:
    #     print('At step 1, psi=',psi[:,j],', r_val=',r_val,', ind_new=',ind_new)
    #     for ind in range(len(x_g)**2):
    #         g1temp=ind % len(x_g)
    #         g2temp=int(ind // len(x_g))
    #         print('For ind=',ind,', g1=',g1temp,', g2=',g2temp,', psi1=',wfn(z_g[g1temp],phi_g[g1temp]),
    #               ', psi2=',wfn(z_g[g2temp],phi_g[g2temp]),', psi_g=',psi_g[g1temp,g2temp,:],', p=',p[ind],' p_cum=',p_cum[ind])
    #     print(' ')

    data[j,-2]=g1B
    data[j,-1]=g2B

data[:,1]=x_g[numpy.array(data[:,-2],dtype=int)]
data[:,2]=y_g[numpy.array(data[:,-2],dtype=int)]
data[:,3]=z_g[numpy.array(data[:,-2],dtype=int)]
data[:,4]=x_g[numpy.array(data[:,-1],dtype=int)]
data[:,5]=y_g[numpy.array(data[:,-1],dtype=int)]
data[:,6]=z_g[numpy.array(data[:,-1],dtype=int)]

numpy.savetxt('two_qubit_rotation_1_path_integral.csv',data,header='t, x1, y1, z1, x2, y2, z2, g1, g2',delimiter=',',fmt='%d,%.19f,%.19f,%.19f,%.19f,%.19f,%.19f,%d,%d')

if sys.argv[-1] != 'show':
    sys.exit()

pylab.figure(1)
pylab.clf()
pylab.plot(data[:,0],data[:,1],label='x1')
pylab.plot(data[:,0],data[:,2],label='y1')
pylab.plot(data[:,0],data[:,3],label='z1')
pylab.legend(loc='best')
pylab.ylim(-1,1)
pylab.savefig('plot_spin_1.png')

pylab.figure(2)
pylab.clf()
pylab.plot(data[:,0],data[:,4],label='x2')
pylab.plot(data[:,0],data[:,5],label='y2')
pylab.plot(data[:,0],data[:,6],label='z2')
pylab.legend(loc='best')
pylab.ylim(-1,1)
pylab.savefig('plot_spin_2.png')

# pylab.figure(3)
# pylab.hist(data[:,1])
# pylab.title('x1')

# pylab.figure(4)
# pylab.hist(data[:,2])
# pylab.title('y1')

# pylab.figure(5)
# pylab.hist(data[:,3])
# pylab.title('z1')

# pylab.figure(6)
# pylab.hist(data[:,4])
# pylab.title('x2')

# pylab.figure(7)
# pylab.hist(data[:,5])
# pylab.title('y2')

# pylab.figure(8)
# pylab.hist(data[:,6])
# pylab.title('z2')

pylab.show()
