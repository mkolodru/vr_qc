# Show implementation of quantum Fourier transform for N qubits using Bloch sphere, starting from random momentum state

import numpy
import numpy.random
import sys
import scipy.linalg

def exp_val(psi,obs):
    return numpy.real(psi.conjugate().transpose().dot(obs.dot(psi)))

def wfn(c_th,phi):
    cc=((1.+c_th)/2.)**0.5 # = cos(theta/2)
    ss=((1.-c_th)/2.)**0.5 # = sin(theta/2)
    return numpy.array([cc,ss*numpy.exp(1.j*phi)])

def calc_obs(psi,X_lst,Y_lst,Z_lst):
    vals=numpy.zeros((3*X_lst.shape[0]))
    for site in range(len(X_lst)):
        vals[3*site]=exp_val(psi,X_lst[site,:,:])
        vals[3*site+1]=exp_val(psi,Y_lst[site,:,:])
        vals[3*site+2]=exp_val(psi,Z_lst[site,:,:])
    return vals

narg=2
if len(sys.argv) < narg:
    print('Usage: fourier_transform_for_grant.py N [rand_seed]')
    sys.exit()

N=int(sys.argv[1])
if len(sys.argv) > 2:
    numpy.random.seed(int(sys.argv[2]))
k0=numpy.random.randint(2**N)

X=numpy.array([[0,1],[1,0]],dtype=complex)
Y=numpy.array([[0,-1.j],[1.j,0]],dtype=complex)
Z=numpy.array([[1,0],[0,-1]],dtype=complex)
I=numpy.array([[1,0],[0,1]],dtype=complex)
proj_0=numpy.array([[1,0],[0,0]],dtype=complex)
proj_1=numpy.array([[0,0],[0,1]],dtype=complex)
H=(X+Z)/2.0**0.5

H_lst=numpy.zeros((N,2**N,2**N),dtype=complex)
X_lst=numpy.zeros((N,2**N,2**N),dtype=complex)
Y_lst=numpy.zeros((N,2**N,2**N),dtype=complex)
Z_lst=numpy.zeros((N,2**N,2**N),dtype=complex)
C_rot_lst=numpy.zeros((N,N,2**N,2**N),dtype=complex)

for site in range(N):
    if site==0:
        HH=H+0
        XX=X+0
        YY=Y+0
        ZZ=Z+0
    else:
        HH=I+0
        XX=I+0
        YY=I+0
        ZZ=I+0
    for j in range(1,N):
        if site==j:
            HH=numpy.kron(HH,H)
            XX=numpy.kron(XX,X)
            YY=numpy.kron(YY,Y)
            ZZ=numpy.kron(ZZ,Z)
        else:
            HH=numpy.kron(HH,I)
            XX=numpy.kron(XX,I)
            YY=numpy.kron(YY,I)
            ZZ=numpy.kron(ZZ,I)
    H_lst[site,:,:]=HH
    X_lst[site,:,:]=XX
    Y_lst[site,:,:]=YY
    Z_lst[site,:,:]=ZZ

for site1 in range(N):
    for site2 in range(site1+1,N):
        phase=numpy.exp(-2.j*numpy.pi/2.0**(site2-site1+1))
        rmat=numpy.array([[1,0],[0,phase]],dtype=complex)
        if site1==0:
            RR=rmat+0
        else:
            RR=I+0
        for j in range(1,N):
            if site1==j:
                RR=numpy.kron(RR,rmat)
            elif site2==j:
                RR=numpy.kron(RR,proj_1)+numpy.kron(numpy.identity(RR.shape[0]),proj_0) # Controlled rotation
            else:
                RR=numpy.kron(RR,I)
        C_rot_lst[site1,site2,:,:]=RR

data=numpy.zeros((N*(N+1)//2+1,3*N+1)) # t, x1, y1, z1, x2, y2, z2, ...
data[:,0]=numpy.arange(data.shape[0])

# psi=numpy.exp(2.j*numpy.pi*k0*numpy.arange(2**N)/2.0**N)/2.0**(N/2) # k0 momentum eigenstate
# Do superposition of k0, 2k0, and 3k0
psi=numpy.exp(2.j*numpy.pi*k0*numpy.arange(2**N)/2.0**N)/2.0**(N/2)/3.0**0.5 
psi=psi+numpy.exp(2*2.j*numpy.pi*k0*numpy.arange(2**N)/2.0**N)/2.0**(N/2)/3.0**0.5 
psi=psi+numpy.exp(3*2.j*numpy.pi*k0*numpy.arange(2**N)/2.0**N)/2.0**(N/2)/3.0**0.5 
psi_t=numpy.zeros((2**N,data.shape[0]),dtype=complex) # |psi(t)> stored for later use
psi_t[:,0]=psi
data[0,1:]=calc_obs(psi,X_lst,Y_lst,Z_lst)
data_ind=1

for site1 in range(N):
    # Hadamard on site 1
    psi=H_lst[site1,:,:].dot(psi)
    psi_t[:,data_ind]=psi
    data[data_ind,1:]=calc_obs(psi,X_lst,Y_lst,Z_lst)
    data_ind+=1
    for site2 in range(site1+1,N):
        # C_rot on site1 controlled by site2
        psi=C_rot_lst[site1,site2,:,:].dot(psi)
        psi_t[:,data_ind]=psi
        data[data_ind,1:]=calc_obs(psi,X_lst,Y_lst,Z_lst)
        data_ind+=1

fname='qft_'+str(N)+'_bloch.csv'
head='t'
form='%d'
for site in range(1,N+1):
    head=head+', x'+str(site)+', y'+str(site)+', z'+str(site)
    form=form+',%.19f,%.19f,%.19f'
numpy.savetxt(fname,data,header=head,fmt=form)

numpy.savetxt('psi_t_real.out',numpy.real(psi_t))
numpy.savetxt('psi_t_imag.out',numpy.imag(psi_t))

if sys.argv[-1] == 'show':
    import pylab

    for site in range(N):
        pylab.figure(site)
        pylab.clf()
        pylab.plot(data[:,0],data[:,3*site+1],label='x'+str(site+1))
        pylab.plot(data[:,0],data[:,3*site+2],label='y'+str(site+1))
        pylab.plot(data[:,0],data[:,3*site+3],label='z'+str(site+1))
        pylab.plot(data[:,0],(data[:,3*site+1]**2+data[:,3*site+2]**2+data[:,3*site+3]**2)**0.5,label='n'+str(site+1))
        pylab.legend(loc='best')
        pylab.ylim(-1.1,1.1)

    pylab.show()
